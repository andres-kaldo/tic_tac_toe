# kas see toimib?
# TIy first commitC_TAC_TOE_BOARD = [0, 1, 2, 3, 4, 5, 6, 7, 8]  # TTT laud


def print_board():  # laua kujundus, 3x3 maatriks
    print("---------")
    print("Current board: ")
    print(TIC_TAC_TOE_BOARD[0:3])
    print(TIC_TAC_TOE_BOARD[3:6])
    print(TIC_TAC_TOE_BOARD[6:])
    print("---------")


def player_move(player_mark):
    player_position = input(f"Please indicate the position to play as {player_mark}: ")  # input on alati str
    player_position = int(player_position)  # teeme str -> int
    TIC_TAC_TOE_BOARD.remove(player_position)
    TIC_TAC_TOE_BOARD.insert(player_position, player_mark)


print_board()

# esimene mängija on "O", teine on "X"
player_1 = " X "
player_2 = " O "
player_1_position = input("please indicate the position to play as 'x': ")  # input on alati str
player_1_position = int(player_1_position)  # teeme str -> int

TIC_TAC_TOE_BOARD.remove(player_1_position)
TIC_TAC_TOE_BOARD.insert(player_1_position, "X")

print_board()
player_2_position = input("please indicate the position to play as 'o': ")  # input on alati str
player_2_position = int(player_2_position)  # teeme str -> int
TIC_TAC_TOE_BOARD.remove(player_2_position)
TIC_TAC_TOE_BOARD.insert(player_2_position, "o")

print_board()
